
## 0.2.0 [08-04-2020]

* add encrypt capability to the connextion

See merge request itentialopensource/adapters/persistence/adapter-db_mssql!1

---

## 0.1.1 [05-20-2020]

* Bug fixes and performance improvements

See commit f5ac952

---
